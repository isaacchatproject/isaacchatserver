package application;

import java.io.IOException;

import com.vdurmont.emoji.EmojiParser;

import server.IsaacChatServer;

public class IsaacChatMain {
	public static void main(String[] args) throws IOException {
		IsaacChatServer isaacChatServer = new IsaacChatServer(8083);
		
		new Thread(isaacChatServer).start();
		
		System.out.println(EmojiParser.parseToUnicode("IsaacChat initialized! :rocket:"));
	}
}
