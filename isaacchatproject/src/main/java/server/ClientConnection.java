package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import com.vdurmont.emoji.EmojiParser;

public class ClientConnection implements Runnable {

	private Socket socket;
	private static PrintWriter clientPrinter;
	public static List<Socket> clients = new LinkedList<>();

	public ClientConnection(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		try {
			String userMessage;
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			// Save the User socket
			clients.add(socket);
			System.out.println(EmojiParser.parseToUnicode("A new user has connected :star:"));
			System.out.println(String.format("IP Address: ", socket.getInetAddress()));

			while ((userMessage = bufferedReader.readLine()) != null) {
				broadcastMessage(userMessage);
			}
		} catch (IOException e) {
			System.out.println(String.format("Client %s exit the chat", socket.getInetAddress()));
		}
	}

	/**
	 * Method to server send message to all clients
	 */
	private void broadcastMessage(String message) {
		for (Socket client : clients) {
			try {
				/**
				 * Open the output stream
				 */
				clientPrinter = new PrintWriter(client.getOutputStream(), true);
				clientPrinter.println(message);
			} catch (IOException e) {
				System.out.println(EmojiParser
						.parseToUnicode(String.format(":boom: An error was encountered: %s", e.getMessage())));
				System.exit(1);
			}
		}
	}
}
