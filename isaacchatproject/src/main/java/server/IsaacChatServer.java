package server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * A SocketServer using in IsaacChat
 *
 * @author Felipe
 * 
 */
public class IsaacChatServer implements Runnable {

	private ServerSocket embeddedServer;

	public IsaacChatServer(Integer socketPort) throws IOException {
		embeddedServer = new ServerSocket(socketPort);
	}

	public IsaacChatServer(InetAddress socketAddress, Integer socketPort) throws IOException {
		embeddedServer = new ServerSocket(socketPort, 0, socketAddress);
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {

			try {
				Socket socket = embeddedServer.accept();

				/**
				 * setTcpNoDelay: Disable the Nagle algorithm, to make the little package
				 * connections better
				 * 
				 * setPerformancePreferences(connectiontime, latency, bandwidth); In this case
				 * (1, 0, 0) I prefered high velocity and low latency
				 */
				socket.setTcpNoDelay(false);
				socket.setPerformancePreferences(1, 0, 0);

				new Thread(new ClientConnection(socket)).start();
			} catch (IOException e) {
				System.out.println(String.format("Was not possible accept the client connection: %s", e.getMessage()));
			}
		}
	}
}
